package com.goldeneyes.spark

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import java.util.Date
import java.sql.Timestamp


object App {

  def main(args: Array[String]) {

    //dev pour execution local
    //preprod pour execution sur prod GE
    //prod pour environnement amazon => mettre cles sauv sur evernote
    val mode = "dev"//prod ou dev

    var fileInPath = "";
    var fileOutPath = "";
    
    println(args);
    
    if (mode == "preprod" || mode == "prod") {
      if (args.length < 2) {
        println("usage com.goldeneyes.spark.App $fileIn $fileOut");
        System.err.println("usage com.goldeneyes.spark.App $fileIn $fileOut");
        System.exit(1)
      }
      fileInPath=args(0);
      fileOutPath=args(1);
    } else if(mode=="dev") {
      fileInPath = "Z:\\BigData\\Trajectoire_Client\\calcul_delai\\input_calcul_delai.csv";
      fileOutPath = "Z:\\BigData\\Trajectoire_Client\\calcul_delai\\output_calcul_delai1.csv";
    }

    val masterConf = "local[*]";
    /*
     * Fichier sous la forme 
     * transactionId, personId, date
     */

    // Config context
    val sconf = new SparkConf().setAppName("calcul_coeff_variation")
    if (mode == "dev") {
      sconf.set("spark.ui.port", "4141").setMaster(masterConf);
    }
    
    val sc = new SparkContext(sconf);

    if(mode == "prod"){
    	//val conf = new SparkConf().setAppName("Simple Application").setMaster("local")  
    	val hadoopConf=sc.hadoopConfiguration;
    	hadoopConf.set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
    	hadoopConf.set("fs.s3.awsAccessKeyId","")
    	hadoopConf.set("fs.s3.awsSecretAccessKey","")
    }

    //Chargement du fichier
    val stringLineList: RDD[String] = sc.textFile(fileInPath);

    //transformation du rdd, on veut personId-timeStamp(24*3600*1000)
//    val dateUtil = new DateUtils;
//    val stringLineListFormated = stringLineList.map(l => l.split(",")).map(a => (a(1), dateUtil.convertDateToLong(a(2)) ));
     val stringLineListFormated = stringLineList.map(l => l.split(",")).map(a => (a(1), convertDateToDayLong(a(2)).toLong));
     
    //transformation, on veut idPerson (tr1,tr2,tr3);
     val reducePersonDateTransaction = stringLineListFormated.groupByKey()//.persist(StorageLevel.MEMORY_AND_DISK);
     
     //supprime doublons (transactions de même jour)
     val reducePersonDateTransactionDistinct = reducePersonDateTransaction.map(s => (s._1, s._2.toSet));

    //filtre plus de 3 transactions de jours différents 
    //val reducePersonDateTransactionFiltered = reducePersonDateTransactionDistinct.filter(s => s._2.size > 2);

    //on transforme en liste idPerson=>List[TransactionDate]
    val reducePersonDateTransactionFilteredCalculated = reducePersonDateTransactionDistinct.map(r => (r._1, computeAll(r._2.toArray))).sortByKey();

    if (mode == "dev") {
      //reducePersonDateTransactionFilteredCalculated.foreach(println)    
      reducePersonDateTransactionFilteredCalculated.repartition(1).saveAsTextFile(fileOutPath);
    } else if (mode == "preprod" || mode == "prod") {
      //reducePersonDateTransactionFilteredCalculated.saveAsTextFile(fileOutPath);
    }
    sc.stop();
  }
  
  
  

  /**
   * Calcul du coeff de variation
   *
   * @param : List des jours de transactions.
   *
   * @return List[String] => ['delai reachat','Variance','Ecart Type','coeff de variation']
   */
  //def computeAll(transactionDateList: Array[Long]): List[String] = {
   def computeAll(transactionDateList: Array[Long]): String = {
       
     
    //il faut au moins 3 dates pour obtenir 2 delais
    if (transactionDateList.size < 3) {
     //return List("0", "0", "0", "0"); 
     return "0" +","+ "0"+","+"0"+","+ "0";
    }
    
    
    //tri par ordre chronologique
    val transactionDateListSorted = transactionDateList.sortWith(_ < _)    
    
    /*
     * composition de la liste des delais
     */
    var delaiList: List[Long] = List();
    for (i <- 0 to transactionDateListSorted.size - 2) {
      var delai: Long = transactionDateListSorted(i + 1) - transactionDateListSorted(i);
      //si achat meme journée, ça ne compte pas.
      if (delai >= 0) {
        //delaiList:+delai;
        delaiList = delai :: delaiList
        //println("ajout delai:"+delai+" i:"+i+" transactionDateList.size:"+transactionDateList.size);
      }
    }
//    println("delaiList size:" + delaiList.size);
   /* if (delaiList.size < 2) {
    //return List("0", "0", "0", "0");
     return "0" +","+ "0"+","+"0"+","+ "0";
    }*/

    var delai = delaiReachat(delaiList)
    var variance = distanceDelaVariance(delaiList)
    var ecartType = Math.sqrt(variance);
    var coeffDeVariation = {
      if (delai!=0) ecartType / delai
      else 0
    }

    //return List(delai.toString(), variance.toString(), ecartType.toString(), coeffDeVariation.toString())
    return delai.toString()+","+ variance.toString()+","+ ecartType.toString()+","+ coeffDeVariation.toString()
  }

  /**
   * Calcule le delai de la variance
   * @param delaiList : liste des delais en jours.
   *
   * return double value du delai de reachat
   */
  def distanceDelaVariance(delaiList: List[Long]): Double = {
    var total: Double = 0;
    for (delai <- delaiList) {
      var result = delai-this.delaiReachat(delaiList)
      total += result*result
    }
    return total / delaiList.size;
  }

  /**
   * Calcule le delai de reachat
   * @param delaiList : liste des delais en jours.
   *
   * return double value du delai de reachat
   */
  def delaiReachat(delaiList: List[Long]): Double = {
    var total: Double = 0;
    for (delai <- delaiList) {
      total += delai
    }
    return total / delaiList.size;
  }
  
  

   /**
   * format 2010-01-21 17:32:00
   * @param dateString : 2010-01-21 17:32:00
   * @return timestamp en unité jour
   * @throws ParseException
   */
  
  def convertDateToDayLong(dateString: String): Double = {
     var format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
     var timeToDay=(24*3600*1000);
     var date: Date = new Date();  
 
    try {
      
        date = format.parse(dateString);
        return date.getTime / timeToDay;
     
    } catch {
      case e: Exception => println("impossible de traiter : " + dateString); return 0;
    }
  }
  
}