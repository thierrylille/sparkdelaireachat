package com.goldeneyes.spark

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import java.util.Date
import java.util.Calendar
import java.sql.Timestamp

object test {
  
  def main(args: Array[String]) {
    
    var time  = convertDateToDayLong("2013-02-01 12:35:00").toLong
    println(time);
    var time2  = convertDateToDayLong("2013-02-02 12:50:00").toLong
    println(time2);
    var datetime = new Date(time.toLong)
    println(datetime)
    var newdate = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(datetime)
    println(newdate)
    var date = convertDateToDayLong(newdate)
    println(date)
  }
  
   def convertDateToDayLong(dateString: String): Double = {
     var format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
     var timeToDay=(24*3600*1000);
     var date: Date = new Date();  
 
    try {
      
        date = format.parse(dateString);
        return date.getTime.toDouble / timeToDay;
     
    } catch {
      case e: Exception => println("impossible de traiter : " + dateString); return 0;
    }
  }
}